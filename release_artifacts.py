#!/usr/bin/env python3

import argparse
import base64
import os
from release import ArtifactoryRelease, DockerRelease, release_artifacts
from registry import initialize_docker_client, DockerRegistry
from typing import Any, Dict
from urllib.parse import urljoin
from yaml import load, dump, Loader, Dumper


BASE_ARTIFACTORY_URL = "https://transparentinc.jfrog.io/artifactory/"
PRE_RELEASE_ARTIFACTORY_URL = urljoin(BASE_ARTIFACTORY_URL, "artifacts-internal/")
RELEASE_ARTIFACTORY_URL = urljoin(BASE_ARTIFACTORY_URL, "artifacts-external/")

GOOGLE_CLOUD_DOCKER_REGISTRY = "gcr.io/xand-dev/"
ARTIFACTORY_DOCKER_REGISTRY = "transparentinc-docker-external.jfrog.io/"

ARTIFACTORY_USERNAME = "artifatory_username"
ARTIFACTORY_PASSWORD = "artifatory_password"
BASE64_GCLOUD_DEV_SERVICE_KEY = "base64_gcloud_dev_service_key"
CI_JOB_URL="ci_job_url"
CI_BRANCH="ci_branch"
CI_PIPELINE_ID="ci_pipeline_id"
DRY_RUN = "dry_run"
SOFTWARE_VERSION_CONFIG = "software_version_config"
DEVOPS_VERSION_CONFIG = "devops_version_config"
OUTPUT_DIR = "output_dir"


def main():
    """The main function which will take care of all the steps to be able to release artifacts externally.
    """
    args = _get_args()
    artifactory_username = args[ARTIFACTORY_USERNAME]
    artifactory_password = args[ARTIFACTORY_PASSWORD]
    service_key_bytes = args[BASE64_GCLOUD_DEV_SERVICE_KEY].encode("UTF-8")
    ci_job_url = args[CI_JOB_URL]
    ci_branch = args[CI_BRANCH]
    ci_pipeline_id = args[CI_PIPELINE_ID]
    dry_run = args[DRY_RUN]
    software_version_config_path = args[SOFTWARE_VERSION_CONFIG]
    devops_version_config_path = args[DEVOPS_VERSION_CONFIG]
    output_dir = args[OUTPUT_DIR]

    if os.path.exists(output_dir):
        raise Exception(f"The output dir '{output_dir}' should not exist since contents will be wiped.")

    os.mkdir(output_dir)

    service_key_decoded = base64.decodebytes(service_key_bytes)
    service_key_contents = service_key_decoded.decode("UTF-8")
    artifactory_registry = DockerRegistry(
        registry=ARTIFACTORY_DOCKER_REGISTRY,
        username=artifactory_username,
        password=artifactory_password
    )
    gcr_registry = DockerRegistry(
        registry=GOOGLE_CLOUD_DOCKER_REGISTRY,
        username="_json_key",
        password=service_key_contents
    )

    with open(software_version_config_path, "rb") as f:
        software_versions = load(f, Loader=Loader)

    with open(devops_version_config_path, "rb") as f:
        devops_versions = load(f, Loader=Loader)

    all_versions = { **software_versions, **devops_versions }

    docker_client = initialize_docker_client(gcr_registry, artifactory_registry)
    print(f"Pre-release url: {PRE_RELEASE_ARTIFACTORY_URL}")
    artifactory_releaser = ArtifactoryRelease(
        username=artifactory_username,
        password=artifactory_password,
        pre_release_url=PRE_RELEASE_ARTIFACTORY_URL,
        release_url=RELEASE_ARTIFACTORY_URL,
        dry_run=dry_run,
        ci_job_url=ci_job_url,
        ci_branch=ci_branch,
        ci_pipeline_id=ci_pipeline_id
    )
    docker_releaser = DockerRelease(
        username=artifactory_username,
        password=artifactory_password,
        pre_release_registry=gcr_registry,
        release_registry=artifactory_registry,
        client=docker_client,
        dry_run=dry_run
    )

    release_artifacts(
        artifactory_releaser,
        docker_releaser,
        all_versions,
        output_dir
    )


def _get_args() -> Dict[str, Any]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """

    parser = argparse.ArgumentParser(description="Release artifacts for public use.")

    parser.add_argument("--artifactory-username",
                        type=str,
                        help="The Artifactory username of the user that has permission to release artifacts. [ARTIFACTORY_USER]",
                        default=os.environ.get('ARTIFACTORY_USER'))

    parser.add_argument("--artifactory-password",
                        type=str,
                        help="The Artifactory password of the user that has permission to release artifacts. [ARTIFACTORY_PASS]",
                        default=os.environ.get('ARTIFACTORY_PASS'))

    parser.add_argument("--base64-gcloud-dev-service-key",
                        type=str,
                        help="The base64 encoded google cloud dev service account's key for pulling pre-release image from gcr. [GCLOUD_DEV_SERVICE_KEY]",
                        default=os.environ.get('GCLOUD_DEV_SERVICE_KEY'))

    parser.add_argument("--ci-job-url",
                        type=str,
                        help="The job url that produced all these artifacts.[CI_JOB_URL]",
                        default=os.environ.get('CI_JOB_URL'))

    parser.add_argument("--ci-branch",
                        type=str,
                        help="The git branch that produced all these artifacts.[CI_BRANCH]",
                        default=os.environ.get('CI_BRANCH'))

    parser.add_argument("--ci-pipeline-id",
                        type=str,
                        help="The pipeline id that produced all these artifacts.[CI_PIPELINE_ID]",
                        default=os.environ.get('CI_PIPELINE_ID'))

    parser.add_argument("--dry-run",
                        help="Whether to run this in dry-run mode which won't release artifacts and just print messages as an example.",
                        action="store_true")

    parser.add_argument("software_version_config",
                        type=str,
                        metavar="SOFTWARE_VERSION_CONFIG",
                        help="The path to the software version config that has the versions of the software to release.")

    parser.add_argument("devops_version_config",
                        type=str,
                        metavar="DEVOPS_VERSION_CONFIG",
                        help="The path to the devops version to release.")

    parser.add_argument("output_dir",
                        type=str,
                        metavar="OUTPUT_DIR",
                        help="The output folder where the deployment artifacts will land.")

    args = parser.parse_args()
    if not args.artifactory_username or len(args.artifactory_username) == 0 or \
       not args.artifactory_password or len(args.artifactory_password) == 0 or \
       not args.base64_gcloud_dev_service_key or len(args.base64_gcloud_dev_service_key) == 0:

        parser.print_usage()
        exit(1)

    return {
        ARTIFACTORY_USERNAME: args.artifactory_username,
        ARTIFACTORY_PASSWORD: args.artifactory_password,
        BASE64_GCLOUD_DEV_SERVICE_KEY: args.base64_gcloud_dev_service_key,
        CI_JOB_URL: args.ci_job_url,
        CI_BRANCH: args.ci_branch,
        CI_PIPELINE_ID: args.ci_pipeline_id,
        DRY_RUN: args.dry_run,
        SOFTWARE_VERSION_CONFIG: args.software_version_config,
        DEVOPS_VERSION_CONFIG: args.devops_version_config,
        OUTPUT_DIR: args.output_dir
    }


if __name__ == "__main__":
    main()
