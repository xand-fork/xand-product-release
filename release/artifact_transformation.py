# How to create interfaces in python is to create an abstract class with method stubs that throw NotImplementedError.
class ArtifactTransformationInterface:

    def transform(self, working_dir: str, artifact_identifier: str) -> str:
        """Performs a transformation on the object specified based on the implementation class of this transformer interface.

        :param working_dir: The working directory for the transformation to publish artifacts from.
        :type working_dir: str
        :param artifact_identifier: A way to identify the artifact in the case of a zip that will be a filepath and in the
            case of a docker image it'll be the image name with version.
        :type artifact_identifier: str

        :returns: An identifier after the transformation has completed to use for publishing
        :rtype: str
        """
        raise NotImplementedError
